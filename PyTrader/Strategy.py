"""
Strategy 클래스는 자동 매매 종목을 선정하기 위한 전략들로 구성되어 있습니다.

author: 권순규
last edit: 2020-07-24
수정:
수정일:
"""
from PyQt5.QtCore import QDate
import csv
import time
import pandas as pd


class Strategy():

    def __init__(self, kiwoom):
        self.kiwoom = kiwoom

    def getKOSPIMovingAverage(self, movingDays, totalBuyPrice, account, passwd):
        """ 코스피 이평선이 상승반전 일때 레버리지를 매수하고 인버스를 매도한다. 하락반전시 레버리지를 매도하고 인버스를 매수한다. """
        strategyName = f"코스피{movingDays}이평선"

        # 매도 수량을 정하기 위해 계좌평가잔고 조회
        self.kiwoom.setInputValue("계좌번호", account)
        self.kiwoom.setInputValue("비밀번호", passwd)
        self.kiwoom.commRqData("계좌평가잔고내역요청", "opw00018", 0, "2000")

        while self.kiwoom.inquiry == '2':
            time.sleep(0.2)

            self.kiwoom.setInputValue("계좌번호", account)
            self.kiwoom.setInputValue("비밀번호", passwd)
            self.kiwoom.commRqData("계좌평가잔고내역요청", "opw00018", 2, "2")

        # 코스피 120 이평선 계산
        self.kiwoom.setInputValue("업종코드", "001")
        self.kiwoom.setInputValue("기준일자", QDate.currentDate().toString("yyyyMMdd"))
        self.kiwoom.commRqData("업종일봉조회요청", "opt20006", 0, "2000")

        self.kiwoom.opt20006Data = self.kiwoom.opt20006Data[["일자", "현재가"]]
        self.kiwoom.opt20006Data = self.kiwoom.opt20006Data.sort_values("일자", ascending=True)

        self.kiwoom.opt20006Data[f"{movingDays}이평선"] = self.kiwoom.opt20006Data["현재가"].rolling(movingDays).mean()

        # 최근 3거래일간의 movingDays 이평선 값 추출
        today, before_1_tradedays, before_2_tradedays = self.kiwoom.opt20006Data[f"{movingDays}이평선"].tail(3).values

        leverageCode = "122630"
        leverageCodeName = self.kiwoom.getMasterCodeName(leverageCode)
        inverseCode = "114800"
        inverseCodeName = self.kiwoom.getMasterCodeName(inverseCode)

        # 오늘의 movingDays 이평선이 어제의 movingDays 이평선보다 크고, 어제의 movingDays 이평선이 그제의 movingDays 이평선보다 작으면 => 오늘 movingDays 이평선이 상승반전하면
        if (today > before_1_tradedays) and before_1_tradedays < before_2_tradedays:
            # 레버리지 매수, 인버스 매도
            inverseBalance = self.kiwoom.opw00018Data['stocks'].query(f"종목명 == '{inverseCodeName}'")
            time.sleep(1)
            self.kiwoom.commKwRqData(leverageCode, 0, 1, "관심종목정보요청", "2000")
            leveragePrice = self.kiwoom.OPTKWFIDData["현재가"]
            leverageQuantity = totalBuyPrice // leveragePrice
            with open("stock.csv", "at", encoding="utf-8", newline="") as f:
                wr = csv.writer(f)
                wr.writerow([leverageCode, leverageCodeName, "매수", "매수전", "시장가", 0, leverageQuantity,
                             QDate.currentDate().toString("yyyyMMdd"), None, None, 0, 0, None, None,
                             strategyName])
                wr.writerow([inverseCode, inverseCodeName, "매도", "매도전", "시장가", 0, inverseBalance,
                             QDate.currentDate().toString("yyyyMMdd"), None, None, 0, 0, None, None,
                             strategyName])
        # 오늘의 movingDays 이평선이 어제의 movingDays 이평선보다 작고, 어제의 movingDays 이평선이 그제의 movingDays 이평선보다 크면 => 오늘 movingDays 이평선이 하락반전하면
        elif (today < before_1_tradedays) and (before_1_tradedays > before_2_tradedays):
            # 인버스 매수, 레버리지 매도
            leverageBalance = self.kiwoom.opw00018Data['stocks'].query(f"종목명 == '{leverageCodeName}'")
            time.sleep(1)
            self.kiwoom.commKwRqData(inverseCode, 0, 1, "관심종목정보요청", "2000")
            inversePrice = self.kiwoom.OPTKWFIDData["현재가"]
            inverseQuantity = totalBuyPrice // inversePrice
            with open("stock.csv", "at", encoding="utf-8", newline="") as f:
                wr = csv.writer(f)
                wr.writerow([leverageCode, leverageCodeName, "매도", "매도전", "시장가", 0, leverageBalance,
                             QDate.currentDate().toString("yyyyMMdd"), None, None, 0, 0, None, None,
                             strategyName])
                wr.writerow([inverseCode, inverseCodeName, "매수", "매수전", "시장가", 0, inverseQuantity,
                             QDate.currentDate().toString("yyyyMMdd"), None, None, 0, 0, None, None,
                             strategyName])

        self.kiwoom.opwDataReset()

    def dualMomentumSmartBeta(self, account, passwd, momentumMonth=6, rebalancingMonth=1):
        '''
        KINDEX 스마트베타 (로우볼, 퀄리티, 밸류, 모멘텀) 4 종목으로 듀얼 모멘텀 전략
        - 스마트베타 중 한 종목만 보유하는 것으로 가정함.
        '''
        strategyName = f"스마트베타듀얼모멘텀_{momentumMonth}_{rebalancingMonth}"

        # rebalancingMonth 주기로 코드 진행
        # TODO: 월만 계산할 게 아니라, 해당 월의 첫번째 거래일로 해야함. 단순 1일로 하기에는 휴장일일 가능성이 있음.
        if (QDate.currentDate().month() - 1) % rebalancingMonth == 0:
            dualMomentumSmartBeta = pd.read_csv("dualMomentumSmartBeta.csv").sort_values(["year", "month"], ascending=False)
            # 마지막으로 저장된 정보 추출
            lastData = dualMomentumSmartBeta.query("momentumMonth == @momentumMonth & rebalancingMonth == @rebalancingMonth")[["year", "month"]].values.flatten()
            # 마지막으로 저장된 정보가 없으면, (year, month)를 0으로 초기화 시켜 전략이 진행되도록
            if len(lastData) == 0:
                year = month = 0
            # 마지막으로 저장된 정보가 두 개면, 마지막 입력된 연월과 현재를 비교해서 전략이 진행되도록
            elif len(lastData) == 2:
                year, month = lastData
            else:
                print(lastData)
                raise Exception()

            # 마지막으로 입력된 연월이 현재와 다르면 리밸런싱 시작
            if year != QDate.currentDate().year() or month != QDate.currentDate().month():
                # 4가지 스마트베타 모멘텀기간의 수익률 계산
                smartBeta = {"322130": "KINDEX 스마트로우볼", "272220": "KINDEX 스마트모멘텀", "322120": "KINDEX 스마트퀄리티",
                             "272230": "KINDEX 스마트밸류"}
                yieldGap = {}
                for code, codeName in smartBeta.items():
                    self.kiwoom.setInputValue("종목코드", code)
                    self.kiwoom.setInputValue("기준일자", QDate.currentDate().toString("yyyyMMdd"))
                    self.kiwoom.commRqData("주식일봉차트조회요청", "opt10081", 0, "2000")

                    self.kiwoom.opt10081Data = self.kiwoom.opt10081Data[["일자", "현재가"]]

                    currentPrice = self.kiwoom.opt10081Data.loc[0, "현재가"]
                    basePrice = self.kiwoom.opt10081Data.loc[(20 * momentumMonth) - 1, "현재가"]  # 1달을 20거래일로 가정

                    yieldGap.update({code: (currentPrice - basePrice) / basePrice})

                yieldMaxCode = max(yieldGap.keys(), key=(lambda k: yieldGap[k]))
                yieldMaxCodeName = smartBeta[yieldMaxCode]

                # 보유 중인 스마트 베타 종목 조회
                self.kiwoom.setInputValue("계좌번호", account)
                self.kiwoom.setInputValue("비밀번호", passwd)
                self.kiwoom.commRqData("계좌평가잔고내역요청", "opw00018", 0, "2000")

                while self.kiwoom.inquiry == '2':
                    time.sleep(1)

                    self.kiwoom.setInputValue("계좌번호", account)
                    self.kiwoom.setInputValue("비밀번호", passwd)
                    self.kiwoom.commRqData("계좌평가잔고내역요청", "opw00018", 2, "2")

                # 최고 수익률을 기록한 스마트 베타를 보유 중이라면 = 이전 리밸런싱 기간과 최고 수익률 종목이 같으면
                if yieldMaxCodeName in self.kiwoom.opw00018Data['stocks']['종목명'].values:
                    # 유지
                    pass
                # 보유 중이지 않다면 = 이전 리밸런싱 기간과 최고 수익률 종목이 다르면
                else:
                    with open("stock.csv", "at", encoding="utf-8", newline="") as f:
                        wr = csv.writer(f)
                        # 보유 중인 스마트 베타 종목을 매도하고
                        holdingCodeName = self.kiwoom.opw00018Data['stocks']['종목명'].values
                        for code, codeName in smartBeta.items():
                            if codeName in holdingCodeName:
                                holdingQuantity = self.kiwoom.opw00018Data['stocks'].query(f"종목명 == {codeName}")['보유수량']
                                wr.writerow([code, codeName, "매도", "매도전", "시장가", 0, holdingQuantity,
                                             QDate.currentDate().toString("yyyyMMdd"), None, None, 0, 0, None, None,
                                             strategyName])
                                break  # 한 종목만 보유 중이라고 가정하기 때문에

                        # 최고 수익률을 기록한 스마트 베타 종목 매수
                        time.sleep(1)
                        self.kiwoom.commKwRqData(yieldMaxCode, 0, 1, "관심종목정보요청", "2000")
                        price = self.kiwoom.OPTKWFIDData["현재가"]
                        quantity = int(100_000 // price)
                        wr.writerow([yieldMaxCode, yieldMaxCodeName, "매수", "매수전", "시장가", 0, quantity,
                                     QDate.currentDate().toString("yyyyMMdd"), None, None, 0, 0, None, None,
                                     strategyName])
                # 이번달의 리밸런싱이 완료되었음을 저장
                with open("dualMomentumSmartBeta.csv", "at", encoding="utf-8", newline="") as f:
                    wr = csv.writer(f)
                    wr.writerow([QDate.currentDate().year(), QDate.currentDate().month(), momentumMonth, rebalancingMonth, yieldMaxCodeName])